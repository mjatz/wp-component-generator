<?php
/***
 * ComponentFghj 
 * generted from default blueprint
 * 
 * USAGE
<?php
include_once( locate_template( 'parts/component-fghj/component-fghj'.php' ));
new ComponentFghj(); 
?>
 *
 */

class ComponentFghj{

    private $component_name;

    public function __construct(){


            $this->component_name = 'component-fghj';

            $this->init_dependencies();

            $this->displayComponent();
            
    }
    

    private function init_dependencies(){

           
            wp_enqueue_style( $this->component_name.'-css', get_template_directory_uri() . '/parts/'. $this->component_name. '/'.$this->component_name.'.css', array(), '', 'all' );

            wp_enqueue_script( $this->component_name.'-js', get_template_directory_uri() . '/parts/'. $this->component_name. '/'.$this->component_name.'.js' );

    }

    private function displayComponent(){ ?>

            <div class="<?php echo $this->component_name ?>">

                <?php $this->displaycontent(); ?>
            
            </div>
            
            <?php
    }


    private function displaycontent(){ ?>

          <h1><?php echo $this->component_name ?></h1> 
        
        <?php
    }
} ?>



