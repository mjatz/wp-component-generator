<?php
/***
 * __ComponentClassName__ 
 * generted from default blueprint
 * 
 * USAGE

<?php
include_once( locate_template( 'parts/__component-name__/__component-name__.php' ));
new __ComponentClassName__( get_field('__component-name__') ); 
?>
<?php
require_once( 'parts/__component-name__/__component-name__.php' );
new __ComponentClassName__( get_field('__component-name__') ); 
?>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class __ComponentClassName__{

    private $component_name;

    public function __construct(){


            $this->component_name = '__component-name__';

            $this->init_dependencies();

            //$this->acf_component = $acf_component;

            $this->displayComponent();
            
    }
    

    private function init_dependencies(){

           
            wp_enqueue_style( $this->component_name.'-css', get_template_directory_uri() . '/parts/'. $this->component_name. '/'.$this->component_name.'.css', array(), '', 'all' );

            wp_enqueue_script( $this->component_name.'-js', get_template_directory_uri() . '/parts/'. $this->component_name. '/'.$this->component_name.'.js' );

    }

    private function displayComponent(){ ?>

            <div class="<?php echo $this->component_name ?>">

                <?php $this->displaycontent(); ?>
            
            </div>
            
        <?php
    }


    private function displaycontent(){ ?>

          <h1><?php echo $this->component_name ?></h1>
        
        <?php
    }
} ?>