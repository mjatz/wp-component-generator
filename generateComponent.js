var fs = require('fs');
var Input = require('prompt-input');


createComponent = function (){

      //console.log(process.argv[1]);

      console.log(process);

      // PromptComponentName
      let pcg = new PromptComponentName();

      pcg.doInputPrompt().then( function(componentRawName) {
            
            if(componentRawName){
                  
                  let cg = new ComponentGenerator(componentRawName)

                  // CreateDir
                  cg.createComponentDir()
                   
                  // Create PHP file From Blueprint
                  cg.createComponentPHPClassFile();
                  cg.createComponentLESSFile();
                  //createComponentCSSFile(component);
                 
                 
            }else{
                  console.log('exit - empty input');
            }
      });
     
} 

class PromptComponentName {

      constructor() { 
           
            this.input = new Input({
                  name: 'ask_component_name',
                  message: 'Component name?'
            });

            
      }

      doInputPrompt(){
         
            return this.input.run().then( function(componentRawName) {
                  return componentRawName;
            });
      }
}


class ComponentGenerator {

      constructor(componentRawName) { 

            this.componentRawName = componentRawName;  
            this.prefix = 'component-';

            this.component = {
                  'rawName' : componentRawName,
                  'name' : this.prefix + componentRawName,
                  'phpClassName' : ''
            }

            this.component.phpClassName = this.alterNameToPhpClassName();
            
            this.path = './'+ this.component.name +'/'+ this.component.name
       
      }


      createComponentDir() {

            var dir = './'+ this.component.name;

            if (!fs.existsSync(dir)){


                  // generate folder
                  fs.mkdirSync(dir);

                  console.log('generated directory');
                 
            
            }else{

                  console.log('allready exists');
                  
            }
      }

      createComponentPHPClassFile(){
      
            let extension = '.php';
            var path = this.path + extension;
            var pathOriginal = __dirname + '/blueprints/x-component-blueprint.php';

            var self = this;

            fs.open(path, 'w', function(err, fd) {
                  
                  if (err) {
                        throw 'error opening file: ' + err;
                  }

                  var componentPhpClass = fs.readFileSync(pathOriginal,'utf8').toString('utf8'); 

                  /** Adapt Content */
                  var classAsString = componentPhpClass
                  
                  console.log( self.component );

                  classAsString = componentPhpClass.replace(/__ComponentClassName__/g,  self.component.phpClassName);  
                  classAsString = classAsString.replace(/__component-name__/g,  self.component.name);
                  
                  let bufferOne = Buffer.from( classAsString );

                  fs.write(fd, bufferOne, 0, bufferOne.length, null, function(err) {
                        if (err) throw 'error writing file: ' + err;
                        fs.close(fd, function() {

                              console.log('file written');
                        });
                  });
            });
      }


      createComponentLESSFile(){

            var self = this;

            let extension = '.less';
            var path = this.path + extension;
           
            fs.open(path, 'w', function(err, fd) {
                  if (err) {
                        throw 'error opening file: ' + err;
                  }

                  let lessOutput = '.' + self.component.name+'{ background-color: inherit }'; 
                  let bufferLess = Buffer.from( lessOutput );

                  fs.write(fd, bufferLess, 0, bufferLess.length, null, function(err) {
                        if (err) throw 'error writing file: ' + err;
                        fs.close(fd, function() {

                              console.log('Less file written');
                        })
                  });
            });
      };


      /**
       * genereate php class name - from component name
       */
      alterNameToPhpClassName(){
            
            var firstChar = this.component.name.charAt(0);

            this.component.phpClassName = this.component.name.replace(firstChar, firstChar.toUpperCase() );

            var locatedChars = this.locations('-', this.component.phpClassName);

            //Uppercase char after "-"
            for( var i=0; i < locatedChars.length; i++) {

                  var foundChar = this.component.phpClassName.charAt( locatedChars[i] + 1 );              
                  this.component.phpClassName = this.component.phpClassName.replaceAt( locatedChars[i] + 1, foundChar.toUpperCase() );
            };

            //Remove char "-"
            this.component.phpClassName = this.component.phpClassName.replace(/-/g, '' );

            return this.component.phpClassName;
      }



      locations(substring, string){
            var a=[],i=-1;
            while((i=string.indexOf(substring,i+1)) >= 0) a.push(i);
            return a;
      }

}

String.prototype.replaceAt=function(index, char) {
      var a = this.split("");
      a[index] = char;
      return a.join("");
}
    

// tools.js
// ========
module.exports = {
      createComponent: function () {
            

            createComponent()

            //let pcg = new PromptComponentName();
            //pcg.createComponent(component)

            // let cg = new ComponentGenerator();
            // cg.createComponent(component)
      
      },
      renameComponent: function () {
        // whatever
      }
};
